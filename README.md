### Appunti sui Comandi di Git

#### Configurazione di Git

1. **Configurare l'utente:**

   ```sh
   git config --global user.name "Nome Utente"
   git config --global user.email "email@esempio.com"
   ```

   Configurano il nome utente e l'email globalmente.

2. **Verificare le impostazioni:**
   ```sh
   git config --list
   ```
   Mostra tutte le configurazioni correnti di Git.

#### Creazione e Inizializzazione di un Repository

1. **Inizializzare un nuovo repository:**

   ```sh
   git init
   ```

   Crea un nuovo repository Git nella directory corrente.

2. **Clonare un repository esistente:**
   ```sh
   git clone <url>
   ```
   Crea una copia locale di un repository remoto.

#### Stato del Repository

1. **Verificare lo stato del repository:**

   ```sh
   git status
   ```

   Mostra lo stato dei file nel repository (modificati, non tracciati, in attesa di commit).

2. **Visualizzare le differenze non tracciate:**
   ```sh
   git diff
   ```
   Mostra le differenze tra i file modificati e la loro ultima versione tracciata.

#### Gestione dei File

1. **Aggiungere file all'area di staging:**

   ```sh
   git add <file>
   git add .
   ```

   Aggiunge file specifici o tutti i file modificati all'area di staging, preparandoli per il commit.

2. **Rimuovere file dall'area di staging:**

   ```sh
   git reset <file>
   ```

   Rimuove file specifici dall'area di staging.

3. **Rimuovere file dal repository:**

   ```sh
   git rm <file>
   ```

   Rimuove file dal repository e dall'area di staging.

4. **Rinominare o spostare file:**
   ```sh
   git mv <file_orig> <file_dest>
   ```
   Rinomina o sposta file nel repository.

#### Commit delle Modifiche

1. **Creare un commit:**

   ```sh
   git commit -m "Messaggio del commit"
   ```

   Registra le modifiche nell'area di staging con un messaggio descrittivo.

2. **Saltare l'area di staging:**
   ```sh
   git commit -a -m "Messaggio del commit"
   ```
   Salta l'area di staging, aggiungendo e committando automaticamente tutti i file modificati e tracciati.

#### Log e Cronologia

1. **Visualizzare la cronologia dei commit:**

   ```sh
   git log
   ```

   Mostra la cronologia dei commit nel repository.

2. **Log in una riga:**

   ```sh
   git log --oneline
   ```

   Mostra la cronologia dei commit con un formato compatto.

3. **Log con grafico:**
   ```sh
   git log --graph
   ```
   Mostra la cronologia dei commit con un grafico che rappresenta i branch e i merge.

#### Branching e Merging

1. **Creare un nuovo branch:**

   ```sh
   git branch <nome_branch>
   ```

   Crea un nuovo branch con il nome specificato.

2. **Elencare i branch:**

   ```sh
   git branch
   ```

   Mostra tutti i branch disponibili.

3. **Passare a un branch:**

   ```sh
   git checkout <nome_branch>
   ```

   Passa al branch specificato.

4. **Creare e passare a un nuovo branch:**

   ```sh
   git checkout -b <nome_branch>
   ```

   Crea e passa a un nuovo branch in un solo comando.

5. **Unire un branch nel branch corrente:**

   ```sh
   git merge <nome_branch>
   ```

   Unisce le modifiche del branch specificato nel branch corrente.

6. **Eliminare un branch:**
   ```sh
   git branch -d <nome_branch>
   ```
   Elimina il branch specificato.

#### Collaborazione e Remoti

1. **Aggiungere un remoto:**

   ```sh
   git remote add origin <url>
   ```

   Aggiunge un repository remoto con il nome "origin".

2. **Visualizzare i remoti:**

   ```sh
   git remote -v
   ```

   Mostra gli URL dei repository remoti configurati.

3. **Recuperare i dati dal remoto:**

   ```sh
   git fetch
   ```

   Scarica i dati dal repository remoto senza unirli.

4. **Recuperare e unire i dati dal remoto:**

   ```sh
   git pull
   ```

   Scarica e unisce i dati dal repository remoto nel branch corrente.

5. **Inviare i dati al remoto:**

   ```sh
   git push
   ```

   Carica i commit del branch locale sul repository remoto.

6. **Pushare un branch specifico:**
   ```sh
   git push origin <nome_branch>
   ```
   Carica i commit del branch specificato sul repository remoto.

#### Revert e Reset

1. **Revertire un commit:**

   ```sh
   git revert <hash_commit>
   ```

   Crea un nuovo commit che annulla le modifiche del commit specificato.

2. **Reset dell'area di staging e della working directory:**

   ```sh
   git reset --hard <hash_commit>
   ```

   Ripristina l'area di staging e la working directory allo stato del commit specificato, eliminando tutte le modifiche successive.

3. **Reset solo dell'area di staging:**

   ```sh
   git reset <hash_commit>
   ```

   Ripristina l'area di staging allo stato del commit specificato, mantenendo le modifiche nella working directory.

4. **Reset di file specifici nell'area di staging:**
   ```sh
   git reset <file>
   ```
   Rimuove file specifici dall'area di staging.

#### Stash

1. **Stash delle modifiche:**

   ```sh
   git stash
   ```

   Salva temporaneamente le modifiche non committate, pulendo l'area di lavoro.

2. **Visualizzare gli stash:**

   ```sh
   git stash list
   ```

   Mostra l'elenco degli stash salvati.

3. **Applicare uno stash:**

   ```sh
   git stash apply
   ```

   Applica le modifiche di uno stash all'area di lavoro.

4. **Applicare e rimuovere uno stash:**

   ```sh
   git stash pop
   ```

   Applica le modifiche di uno stash e lo rimuove dall'elenco degli stash.

5. **Eliminare uno stash specifico:**
   ```sh
   git stash drop <stash@{n}>
   ```
   Rimuove uno stash specifico dall'elenco.

#### Altri Comandi Utili

1. **Mostrare i commit che hanno modificato un file:**

   ```sh
   git blame <file>
   ```

   Mostra quale commit ha modificato ciascuna riga di un file.

2. **Visualizzare un file di una versione precedente:**

   ```sh
   git show <hash_commit>:<file>
   ```

   Mostra il contenuto di un file come era in un commit specifico.

3. **Configurare Git per ignorare i file:**

   - Creare un file `.gitignore` e aggiungere i file o le directory da ignorare.
     ```sh
     echo "nomefile_da_ignorare" >> .gitignore
     ```

4. **Configurare gli alias per i comandi:**
   ```sh
   git config --global alias.co checkout
   git config --global alias.br branch
   git config --global alias.ci commit
   git config --global alias.st status
   ```
   Crea alias per i comandi Git per velocizzare l'uso.

#### Pre-commit delle Modifiche

1. **Installare il pacchetto pre-commit:**

   ```sh
   pip install pre-commit
   ```

   Installa pre-commit per gestire gli hook di pre-commit.

2. **Creare il file di configurazione .pre-commit-config.yaml:**

   ```yaml
   repos:
     - repo: https://github.com/pre-commit/pre-commit-hooks
       rev: v4.0.1 # Sostituisci con l'ultima versione
       hooks:
         - id: trailing-whitespace
         - id: end-of-file-fixer
         - id: check-yaml
         - id: check-json
   ```

   Configura gli hook di pre-commit per il repository.

3. **Installare i hook di pre-commit:**

   ```sh
   pre-commit install
   ```

   Installa gli hook di pre-commit nel repository.

4. **Eseguire i hook di pre-commit manualmente:**
   ```sh
   pre-commit run --all-files
   ```
   Esegue i hook di pre-commit su tutti i file.
